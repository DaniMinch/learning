package com.daniminch.example;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

public class AnnoRunTests {

    public static void main(String[] args) throws Exception {
        int tests = 0;
        int passed = 0;
        Class<?> testClass = Class.forName(AnnoRunTests.class.getPackage()
                .getName() + ".AnnoSample");

        for (Method m : testClass.getDeclaredMethods()) {
            if (m.isAnnotationPresent(AnnoTest.class)) {
                tests++;
                if (processTest(m))
                    passed++;
            }
        }
        System.out.printf("Passed: %d, Failed: %d%n",
                passed, tests - passed);
    }

    private static boolean processTest(Method m) throws IllegalAccessException {
        Objects.nonNull(m);
        Class<? extends Throwable> excType =
                m.isAnnotationPresent(AnnoExceptionTest.class)
                        ? m.getAnnotation(AnnoExceptionTest.class).value()
                        : null;
        boolean result = false;

        try {
            m.invoke(null);
            if (excType == null) {
                result = true;
            } else {
                System.out.printf("Test %s failed: no exception%n", m);
            }
        } catch (InvocationTargetException wrappedEx) {
            Throwable exc = wrappedEx.getCause();
            if (excType == null) {
                System.out.printf(
                        "Test %s failed: unexpected exception %s%n",
                        m, exc);
            } else if (!excType.isInstance(exc)) {
                System.out.printf(
                        "Test %s failed: expected %s, got %s%n",
                        m, excType.getName(), exc);
            } else {
                result = true;
            }
        } catch (Exception e) {
            System.out.println("Invalid @Test: " + m);
        }
        return result;
    }

}

