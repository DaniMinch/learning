package com.daniminch.example;

public class Anno {
    @AnnoTest
    public static void m1() {
    } // Test should pass

    public static void m2() {
    }

    @AnnoTest
    public static void m3() { // Test should fail
        throw new RuntimeException("Boom");
    }

    public static void m4() {
    }

    @AnnoTest
    public void m5() {
    } // INVALID USE: nonstatic method

    public static void m6() {
    }

    @AnnoTest
    public static void m7() { // Test should fail
        throw new RuntimeException("Crash");
    }

    public static void m8() {
    }
}
