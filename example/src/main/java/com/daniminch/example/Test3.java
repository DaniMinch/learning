package com.daniminch.example;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Test3 {
    // Wrapper class - uses composition in place of inheritance
    public class InstrumentedSet<E> extends ForwardingSet<E> {
        private int addCount = 0;

        public InstrumentedSet(Set<E> s) {
            super(s);
        }

        @Override
        public boolean add(E e) {
            addCount++;
            return super.add(e);
        }

        @Override
        public boolean addAll(Collection<? extends E> c) {
            addCount += c.size();
            return super.addAll(c);
        }

        public int getAddCount() {
            return addCount;
        }
    }

    // Reusable forwarding class
    public static class ForwardingSet<E> implements Set<E> {
        private final Set<E> s;

        public ForwardingSet(Set<E> s) {
            this.s = s;
        }

        public void clear() {
            s.clear();
        }

        public boolean contains(Object o) {
            return s.contains(o);
        }

        public boolean isEmpty() {
            return s.isEmpty();
        }

        public int size() {
            return s.size();
        }

        public Iterator<E> iterator() {
            return s.iterator();
        }

        public boolean add(E e) {
            return s.add(e);
        }

        public boolean remove(Object o) {
            return s.remove(o);
        }

        public boolean containsAll(Collection<?> c) {
            return s.containsAll(c);
        }

        public boolean addAll(Collection<? extends E> c) {
            return s.addAll(c);
        }

        public boolean removeAll(Collection<?> c) {
            return s.removeAll(c);
        }

        public boolean retainAll(Collection<?> c) {
            return s.retainAll(c);
        }

        public Object[] toArray() {
            return s.toArray();
        }

        public <T> T[] toArray(T[] a) {
            return s.toArray(a);
        }

        @Override
        public boolean equals(Object o) {
            return s.equals(o);
        }

        @Override
        public int hashCode() {
            return s.hashCode();
        }

        @Override
        public String toString() {
            return s.toString();
        }
    }

    // Broken - invokes alien method from synchronized block!
    public static class ObservableSet<E> extends ForwardingSet<E> {
        public ObservableSet(Set<E> set) {
            super(set);
        }

        private final List<Test3SetObserver<E>> observers
                = new ArrayList<>();

        public void addObserver(Test3SetObserver<E> observer) {
            synchronized (observers) {
                observers.add(observer);
            }
        }

        public boolean removeObserver(Test3SetObserver<E> observer) {
            synchronized (observers) {
                boolean var = observers.remove(observer);
                System.out.println("Removed");
                return var;
            }
        }

        private void notifyElementAdded(E element) {
            synchronized (observers) {
                for (Test3SetObserver<E> observer : observers)
                    observer.added(this, element);
            }
        }

        @Override
        public boolean add(E element) {
            boolean added = super.add(element);
            if (added)
                notifyElementAdded(element);
            return added;
        }

        @Override
        public boolean addAll(Collection<? extends E> c) {
            boolean result = false;
            for (E element : c)
                result |= add(element); // Calls notifyElementAdded
            return result;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ObservableSet<Integer> set =
                new ObservableSet<>(new HashSet<>());
        // Observer that uses a background thread needlessly
        set.addObserver(new Test3SetObserver<Integer>() {
            public void added(Test3.ObservableSet<Integer> s, Integer e) {
                System.out.println(e);
                if (e == 23) {
                    ExecutorService exec =
                            Executors.newWorkStealingPool();
                    try {
                        exec.submit(() -> {
                            try {
                                TimeUnit.SECONDS.sleep(1);
                            } catch (InterruptedException ex) {
                                ex.printStackTrace();
                            }
                            System.out.println(Thread.currentThread().isDaemon());
                            s.removeObserver(this);
                        })
//                        .get()
                        ;
//                    } catch (ExecutionException | InterruptedException ex) {
//                        throw new AssertionError(ex);
                    } finally {
                        exec.shutdown();
                    }
                }
            }
        });
        for (int i = 0; i < 100; i++)
            set.add(i);
        System.out.println("finished adding" + set.observers.size());
//        TimeUnit.SECONDS.sleep(1);

    }
}

