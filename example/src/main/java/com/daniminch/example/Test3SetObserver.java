package com.daniminch.example;

@FunctionalInterface
public interface Test3SetObserver<E> {
    // Invoked when an element is added to the observable set
    void added(Test3.ObservableSet<E> set, E element);
}
