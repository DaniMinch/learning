package com.daniminch.example;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.StampedLock;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;


public class Test2 {

    public static class MyClass2 extends MyClass {

        MyClass2(int counter) {
            super(counter);
            foo();
            System.out.println(this.counter);
        }

        private void foo() {
            System.out.println("bar");
        }
    }


    public static class MyClass implements SuperFormula, Comparable<MyClass> {

        MyClass() {
            foo();
        }

        MyClass(int counter) {
            this();
            this.counter = counter;
        }

        @Override
        public double calculateBetter(int a) {
            return a;
        }

        @Override
        public double calculate(int a) {
            return 1;
        }

        Integer counter = 1;

        public synchronized void syncMethod() {
            System.out.println("Starting method lock in thread " + Thread.currentThread().getName()
                    + "with value " + counter);
            this.syncMethod2();
            try {
                TimeUnit.SECONDS.sleep(4);
                System.out.println("Before setting in method lock in thread " + Thread.currentThread().getName()
                        + "with value " + counter);
                counter = 100;

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Finishing method lock with value " + counter);
        }

        public synchronized void syncMethod2() {
            System.out.println("Seconf one");
        }

        public void lockMethod() {
            synchronized (this) {
                System.out.println("Starting this lock in thread " + Thread.currentThread().getName()
                        + "with value " + counter);
                try {
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println("Before setting this lock in thread " + Thread.currentThread().getName()
                            + "with value " + counter);
                    counter *= 4;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Finishing this lock with value " + counter);
        }

        public void lockMethod2() {
            System.out.println("Starting resource lock in thread " + Thread.currentThread().getName()
                    + "with value " + counter);
            synchronized (counter) {
                try {
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println("Starting resource lock in thread " + Thread.currentThread().getName()
                            + "with value " + counter);
                    counter++;
                } catch (InterruptedException e) {
                }
            }
            System.out.println("Finishing resource lock with value " + counter);
        }

        private void foo() {
            System.out.println("Foo");
        }

        @Override
        public int compareTo(MyClass o) {
            return Integer.compare(this.counter, o.counter);
        }
    }

    public static class NewsService {
        public static String getMessage() {
            System.out.println("This is a message");
            return "Message";
        }

        public static String getInfo() {
            sleep(1000);
            System.out.println("This is a Info " + Thread.currentThread().getName());
            System.out.println(2 / 0);
            return "Info";
        }

        public static CompletableFuture<String> supplyInfo() {
            return CompletableFuture.supplyAsync(NewsService::getInfo);
        }

        public static CompletableFuture<String> supplyMessage() {
            return CompletableFuture.supplyAsync(NewsService::getMessage);
        }
    }

    public class Anagrams {
        public void main(String[] args) throws IOException {
            Path dictionary = Paths.get(args[0]);
            int minGroupSize = Integer.parseInt(args[1]);
            try (Stream<String> words = Files.lines(dictionary)) {
                words.collect(
                        groupingBy(word -> word.chars().sorted()
                                .collect(StringBuilder::new,
                                        (sb, c) -> sb.append((char) c),
                                        StringBuilder::append).toString()
                        )
                )
                        .values().stream()
                        .filter(group -> group.size() >= minGroupSize)
                        .map(group -> group.size() + ": " + group)
                        .forEach(System.out::println);
            }
        }
    }

    public static class SubLists {
        public static <E> Stream<List<E>> of(List<E> list) {
            prefixes(list).forEach(System.out::println);
            System.out.println("================");
            suffixes(list).forEach(System.out::println);
            System.out.println("================");
            return Stream.concat(Stream.of(Collections.emptyList()),
                    prefixes(list).flatMap(SubLists::suffixes));
        }

        private static <E> Stream<List<E>> prefixes(List<E> list) {
            return IntStream.rangeClosed(1, list.size())
                    .mapToObj(end -> list.subList(0, end));
        }

        private static <E> Stream<List<E>> suffixes(List<E> list) {
            return IntStream.range(0, list.size())
                    .mapToObj(start -> list.subList(start, list.size()));
        }
    }

    private static Callable task = () -> {
        try {
            TimeUnit.SECONDS.sleep(1);
            String threadName = Thread.currentThread().getName();
            System.out.println("Hello " + threadName);
            return 123;
        } catch (InterruptedException e) {
            throw new IllegalStateException("task interrupted", e);
        }
    };
    public static int value = 0;
    public static AtomicInteger atomic = new AtomicInteger(0);

    private static void sleep(int milliseconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            System.out.println("Exception for Thread " + Thread.currentThread().getName()
                    + ":" + Thread.currentThread().isInterrupted());
        }
    }


    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Runnable task = () -> {
            for (int i = 0; i < 10000; i++) {
                value++;
                atomic.incrementAndGet();
            }
        };
        for (int i = 0; i < 3; i++) {
            new Thread(task).start();
        }
        sleep(1000);
        System.out.println(value);
        System.out.println(atomic.get());


//        repeatText("Hi1", 2000);
        Callable task2 = () -> {
            try {
                TimeUnit.SECONDS.sleep(1);
                return 123;
            } catch (InterruptedException e) {
                throw new IllegalStateException("task interrupted", e);
            }
        };

        Predicate<String> predicate = (s) -> s.length() > 0;

        predicate.test("foo");              // true
        predicate.negate().test("foo");     // false

//        exec01();
//        exec02();
//        exec03();
//        exec04();
//        exec05();
//        exec06();
//        exec07();
//        exec08();
//        exec09();
//        exec10();
//        exec11();
//        exec12();
//        exec13();
//        exec14();
        exec15();

        Test2 app = new Test2();


//        app.goInterfaces();
//        app.goMT();
    }


    private static void exec01() throws InterruptedException {

        System.out.println("This is exec 1 =============================");
        ExecutorService executor2 = Executors.newWorkStealingPool(5);
        List<Callable<String>> callables = Arrays.asList(
                () -> {
                    System.out.println(Thread.currentThread().getName());
                    return "task1";
                },
                () -> {
                    System.out.println(Thread.currentThread().getName());
                    return "task2";
                },
                () -> {
                    System.out.println(Thread.currentThread().getName());
                    return "task3";
                });
        executor2
                .invokeAll(callables)
                .stream()
                .map(future -> {
                    try {
                        return future.get();
                    } catch (Exception e) {
                        throw new IllegalStateException(e);
                    }
                })
                .forEach(System.out::println);

        executor2.shutdown();
    }

    private static void exec02() throws InterruptedException {

        System.out.println("This is exec 2 =============================");
        ScheduledExecutorService executor2 = Executors.newScheduledThreadPool(1);
        Future<Integer> future = executor2.submit(task);
        try {

            System.out.println("future done? " + future.isDone());

            Integer result = future.get();

            System.out.println("future done? " + future.isDone());
            System.out.print("result: " + result);
        } catch (ExecutionException e) {
        }

        executor2.shutdown();
    }

    private static void exec03() throws InterruptedException {

        System.out.println("This is exec 3 =============================");
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        Runnable task = () -> System.out.println("Scheduling: " + System.nanoTime());
        ScheduledFuture<?> future = executor.schedule(task, 3, TimeUnit.SECONDS);

        executor.scheduleAtFixedRate(task, 0, 1, TimeUnit.SECONDS);
        executor.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);

        TimeUnit.MILLISECONDS.sleep(1337);

        long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
        System.out.printf("Remaining Delay: %sms", remainingDelay);
        executor.shutdown();
    }

    private static void exec04() throws InterruptedException {

        System.out.println("This is exec 4 =============================");
        Lock lock = new ReentrantLock();
        Runnable task = () -> {
            lock.lock();
            System.out.println("Thread");
            lock.unlock();
        };
        lock.lock();

        Thread th = new Thread(task);
        th.start();
        System.out.println("main");
        Thread.currentThread().sleep(2000);
        lock.unlock();
    }

    private static void exec05() throws InterruptedException {

        System.out.println("This is exec 5 =============================");

        ExecutorService executor = Executors.newFixedThreadPool(2);
        Map<String, String> map = new HashMap<>();
        StampedLock lock = new StampedLock();

        executor.submit(() -> {
            long stamp = lock.writeLock();
            try {
                sleep(1000);
                map.put("foo", "bar");
            } finally {
                lock.unlockWrite(stamp);
            }
        });

        Runnable readTask = () -> {
            long stamp = lock.readLock();
            try {
                System.out.println(map.get("foo"));
                sleep(1000);
            } finally {
                lock.unlockRead(stamp);
            }
        };

        executor.submit(readTask);
        executor.submit(readTask);
        executor.shutdown();
    }

    private static void exec06() throws InterruptedException {

        System.out.println("This is exec 6 =============================");
//        ExecutorService executor = Executors.newSingleThreadExecutor();
        Thread my = new Thread(() -> {
            long startTime = System.currentTimeMillis();
            int iamcycle = 0;
            while (true) {
                ++iamcycle;
                sleep(500);
                if (iamcycle == 10) {
                    System.out.println("Cycle " + (System.currentTimeMillis() - startTime));
                    return;
                }
            }
        });
        my.start();
        sleep(1);
        System.out.println("Calling Interrupt");
        my.interrupt();
    }

    private static void exec07() throws InterruptedException {

        System.out.println("This is exec 7 =============================");
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long startTime = System.currentTimeMillis();
            int iamcycle = 0;
            while (true) {
                ++iamcycle;
                sleep(500);
                System.out.println("After sleep:" + Thread.currentThread().isInterrupted());
                if (iamcycle == 10) {
                    System.out.println("Cycle " + (System.currentTimeMillis() - startTime));
                    return;
                }
            }
        });
        sleep(1);
        System.out.println("Calling Interrupt");
        executor.shutdownNow();
        executor.shutdownNow();
    }

    static int count = 0;

    private static void exec08() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        StampedLock lock = new StampedLock();
        long start = System.currentTimeMillis();

        executor.submit(() -> {
            long stamp = lock.readLock();
            System.out.println("Exec read lock " + stamp);
            sleep(1000);
            System.out.println("Lock Valid: " + lock.validate(stamp));
            try {
                long newstamp;
                if (count == 0) {
                    newstamp = lock.tryConvertToWriteLock(stamp);
                    while (newstamp == 0L) {
//                        System.out.println("Could not convert to write lock");
                        newstamp = lock.tryConvertToWriteLock(stamp);
//                        System.out.println("New stamp is " + stamp);
                    }
                    System.out.println(System.currentTimeMillis() - start);
                    count = 23;
                }
                System.out.println(count);
            } finally {
                lock.unlock(stamp);
            }
        });
        sleep(500);
        System.out.println("Trying to get lock");
        long stamp = lock.readLock();
        System.out.println("Main write lock " + stamp);
        sleep(1000);

        System.out.println((lock.isReadLocked()));
        System.out.println((lock.isWriteLocked()));
        System.out.println("Stamp is " + stamp);
        lock.unlockRead(stamp);
        System.out.println("Lock unlocked " + lock.validate(stamp) + " " + (System.currentTimeMillis() - start));
        System.out.println((lock.isReadLocked()));
        System.out.println((lock.isWriteLocked()));
//        lock.unlock(stamp);
//        System.out.println((lock.isReadLocked()));
//        System.out.println((lock.isWriteLocked()));
        executor.shutdown();
    }

    private static void exec09() throws Exception {

        System.out.println("This is exec 9 =============================");
        Supplier newsSupplier = () -> NewsService.getMessage();
        long start = System.currentTimeMillis();
//        CompletableFuture<String> writer = NewsService.supplyInfo();
        sleep(2000);
        System.out.println("I am here");
//        CompletableFuture<Void> cf = CompletableFuture.completedFuture("!!")
//                .thenCombine(
//                        CompletableFuture.supplyAsync(NewsService::getMessage),
//                        (a, b) -> {
//                            System.out.println(b + a);
//                            return b + a;
//                        })
//                .thenCompose(t -> writer)
//                .exceptionally(t-> writer)
//                .thenCompose(Function.identity())
//                .thenAccept(result -> System.out.println(result));

        CompletableFuture<String> cf = NewsService.supplyInfo();
        sleep(1500);
        System.out.println(cf.isCompletedExceptionally() + " " +
                cf.isDone());
        for (int i = 0; i < 3; i++) {
            cf = cf.thenApply(CompletableFuture::completedFuture)
                    .exceptionally(t -> NewsService.supplyInfo())
                    .thenCompose((t) -> {
                        System.out.println("I is  - "
                                + (System.currentTimeMillis() - start));
                        return t;
                    })
            ;
            System.out.println("Im here " + (System.currentTimeMillis() - start));
        }
        System.out.println("Waiting till its done");
        while (!cf.isDone()) {
            sleep(100);
        }
        System.out.println("Its done");
        System.out.println(cf.isCompletedExceptionally() + " " +
                cf.isDone());
        cf = cf.exceptionally(t -> NewsService.getMessage());
        cf.thenAccept(System.out::println).join();
        System.out.println(cf.isCompletedExceptionally() + " " +
                cf.isDone());
        sleep(500);
        System.out.println(cf.isCompletedExceptionally() + " " +
                cf.isDone());
        System.out.println(System.currentTimeMillis() - start);
        cf.get();
        System.out.println(System.currentTimeMillis() - start);
        cf.get();
        System.out.println(System.currentTimeMillis() - start);
    }

    private static void exec10() throws Exception {
        final AtomicInteger counter = new AtomicInteger();
        ExecutorService tpe = Executors.newWorkStealingPool(5);
        ForkJoinPool fjp = new ForkJoinPool();
        Callable callable = () -> {
            System.out.println(Thread.currentThread().getName());
            return counter.getAndIncrement();
        };
        RecursiveTask task = new RecursiveTask<Integer>() {
            @Override
            protected Integer compute() {
                System.out.println(Thread.currentThread().getName());
                return counter.getAndIncrement();
            }

            ;
        };

        counter.set(0);
        for (int c = 0; c < 10; c++) {
            System.out.print(tpe.submit(callable).get());
        }
        System.out.println((", counter = " + counter.get()));

        counter.set(0);
        for (int c = 0; c < 10; c++) {
            System.out.print(fjp.submit(callable).get());
        }
        System.out.println((", counter = " + counter.get()));

        counter.set(0);
        for (int c = 0; c < 10; c++) {
            System.out.print(fjp.submit(task).get());
        }
        System.out.println((", counter = " + counter.get()));
    }

    private static void exec11() throws Exception {
        System.out.println("This is exec 11 =============================");
        Phaser phaser = new Phaser();
        // Вызывая метод register, мы регистрируем текущий поток (main) как участника
        phaser.register();
        System.out.println("Phasecount is " + phaser.getPhase());
        testPhaser(phaser);
        testPhaser(phaser);
        testPhaser(phaser);
        // Через 3 секунды прибываем к барьеру и снимаемся регистрацию. Кол-во прибывших = кол-во регистраций = пуск
        Thread.sleep(3000);
        phaser.arriveAndDeregister();
        System.out.println("Phasecount is " + phaser.getPhase());
    }

    // Generic singleton factory pattern
    private static UnaryOperator<Object> IDENTITY_FN = (t) -> t;

    @SuppressWarnings("unchecked")
    public static <T> UnaryOperator<T> identityFunction() {
        return (UnaryOperator<T>) IDENTITY_FN;
    }

    private static void exec12() {
        System.out.println("This is exec 12 =============================");
        String[] strings = {"jute", "hemp", "nylon"};
        UnaryOperator<String> sameString = identityFunction();
        for (String s : strings) {
            System.out.println(sameString.apply(s));
            System.out.println(s.hashCode());
        }
        Number[] numbers = {1, 2.0, 3L};
        UnaryOperator<Number> sameNumber = identityFunction();
        for (Number n : numbers) {
            System.out.println(sameNumber.apply(n).hashCode());
            System.out.println(n.hashCode());
        }
    }

    static <T> T[] toArray(T... args) {
        return args;
    }

    static <T> T[] pickTwo(T a, T b, T c) {
        T[] myvar;
        switch (ThreadLocalRandom.current().nextInt(3)) {
            case 0:
                myvar = toArray(a, b);
                break;
            case 1:
                myvar = toArray(a, c);
                break;
            case 2:
                myvar = toArray(b, c);
                break;
            default:
                myvar = null;
        }
        System.out.println(myvar.getClass());
        return myvar;
//        throw new AssertionError(); // Can't get here
    }

    private static void exec13() throws Exception {
        System.out.println("This is exec 13 =============================");
        String[] attributes = pickTwo("Good", "Fast", "Cheap");

    }

    private static void exec14() {
        List<MyClass> myvar = new ArrayList<>();
        myvar.add(new MyClass(3));
        myvar.add(new MyClass(5));
        myvar.add(new MyClass2(10));
        MyClass a = Collections.max(myvar);
        System.out.println("This is answer " + a.getClass());
        System.out.println(Operation.PLUS.name());
        Optional<Operation> b = Operation.fromString("MINUS");
        b.get().apply(1, 2);
        Operation.PLUS.getClass().getEnumConstants();
    }

    private static void exec15() {
        System.out.println("This is exec 15 =============================");
        Stream<List<Integer>> i = SubLists.of(Arrays.asList(1, 2, 3, 4, 5));
        i.forEach(System.out::println);
    }

    private static void exec16() {
        ExecutorService exec = Executors.newCachedThreadPool();
//        exec.submit(System.out::println);
    }


    public enum Operation {
        PLUS("+") {
            public double apply(double x, double y) {
                return x + y;
            }
        },
        MINUS("-") {
            public double apply(double x, double y) {
                return x - y;
            }
        },
        TIMES("*") {
            public double apply(double x, double y) {
                return x * y;
            }
        },
        DIVIDE("/") {
            public double apply(double x, double y) {
                return x / y;
            }
        };
        private final String symbol;

        Operation(String symbol) {
            this.symbol = symbol;
        }

        @Override
        public String toString() {
            return symbol;
        }

        public abstract double apply(double x, double y);

        public static final Map<String, Operation> stringToEnum =
                Stream.of(values()).collect(
                        toMap(Object::toString, e -> e));

        // Returns Operation for string, if any
        public static Optional<Operation> fromString(String symbol) {
            return Optional.ofNullable(stringToEnum.get(symbol));
        }
    }

    private static void testPhaser(final Phaser phaser) {
        // Говорим, что будет +1 участник на Phaser
        phaser.register();
        // Запускаем новый поток
        new Thread(() -> {
            String name = Thread.currentThread().getName();
            System.out.println(name + " arrived");
            phaser.arriveAndAwaitAdvance(); //threads register arrival to the phaser.
            System.out.println(name + " after passing barrier");
        }).start();
    }


    private void goMT() throws InterruptedException {

        System.out.println("This is a go MT.......................");
        MyClass a = new Test2.MyClass2(3);
        List<Callable<Long>> callables = Arrays.asList(
                () -> {
                    System.out.println("Runner 1 " + Thread.currentThread().getName());
                    a.syncMethod();
                    return null;
                },
                () -> {
                    TimeUnit.MILLISECONDS.sleep(500);
                    System.out.println("Runner 2 " + Thread.currentThread().getName());
                    a.lockMethod();
                    return null;
                },
                () -> {
                    TimeUnit.MILLISECONDS.sleep(1000);
                    System.out.println("Runner 3 " + Thread.currentThread().getName());
                    a.lockMethod2();

                    return null;
                });

        ExecutorService executor = Executors.newCachedThreadPool();
        executor.invokeAll(callables);
        executor.shutdown();
    }

    private void goInterfaces() {
        MyClass mc = new MyClass();
        System.out.println(mc.sqrt(4));
    }

    public static void repeatText(String text, int count) {
        Runnable r = () -> {
            for (int i = 0; i < count; i++) {
                System.out.println(text);
                Thread.yield();
            }
        };
        new Thread(r).start();
    }


}

interface Formula {
    double calculate(int a);

    default double sqrt(int a) {
        return Math.sqrt(a);
    }
}

interface SuperFormula extends Formula {
    double calculateBetter(int a);

    @Override
    default double sqrt(int a) {
        return Math.pow(a, 3);
    }
}