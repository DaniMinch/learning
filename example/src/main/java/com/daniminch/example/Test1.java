package com.daniminch.example;

import java.util.concurrent.TimeUnit;

class Incremenator extends Thread {
    private volatile boolean mIsIncrement = true;

    public void changeAction()    //Меняет действие на противоположное
    {
        mIsIncrement = !mIsIncrement;
    }

    @Override
    public void run() {
        do {
            this.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread t, Throwable e) {
                    System.out.println("This is a thread" + t.getName());
                    System.out.println("This is an uncaughtException " + e.toString());
                }
            });
            MyWorker myself = new MyWorker();
            myself.safeMethod();
            myself.safeMethod();


            if (!Thread.interrupted()) {

                if (mIsIncrement) TestClass.mValue++;
                else TestClass.mValue--;
                //Вывод текущего значения переменной
                System.out.print(TestClass.mValue + " ");
            } else
                return;        //Завершение потока

            try {
                Thread.sleep(1000);        //Приостановка потока на 1 сек.
            } catch (InterruptedException e) {
                return;    //Завершение потока после прерывания
            }
        }
        while (true);
    }
}

class TestClass {
    //Переменая, которой оперирует инкременатор
    public static int mValue = 0;

    static Incremenator mInc;    //Объект побочного потока

    public static void executeMultiThreading() {
        mInc = new Incremenator();    //Создание потока

        System.out.print("Значение = ");
        mInc.start();    //Запуск потока
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
        }
        Runnable task = () -> {
            MyWorker.safeStaticMethod();
        };
        Thread taskThread = new Thread(task);
        taskThread.start();
        // После оповещения нас мы будем ждать, пока сможем взять лок
        System.out.println("thread");


        //Троекратное изменение действия инкременатора
        //с интервалом в i*2 секунд
        for (int i = 1;
             i <= 3; i++) {
            try {
                Thread.sleep(i * 2 * 1000);        //Ожидание в течении i*2 сек.
            } catch (InterruptedException e) {
            }

            mInc.changeAction();    //Переключение действия
        }

        mInc.interrupt();    //Прерывание побочного потока
    }
}

class MyWorker {
    private void foo() {
        System.out.println("putinvor");
    }

    void bar() {
        this.foo();
    }

    static int magicnumber = 42;

    static synchronized void safeStaticMethod() {
        System.out.println("==========================================================");
        System.out.println("Static thread is " + Thread.currentThread().getName() + ". Number is" + magicnumber);
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
        }
        System.out.println("Finishing Static Method" + magicnumber);
    }

    synchronized static void safeMethod() {
        System.out.println("I'm thread " + Thread.currentThread().getName() + ". Number is" + magicnumber++);
    }
}

class MyWorker2 extends MyWorker {

    @Override
    void bar() {
        super.bar();
        System.out.println("123123");
    }
}

class TestClass1 extends TestClass {
    public static void main(String[] args) {
        MyWorker2 a = new MyWorker2();
        a.bar();
        executeMultiThreading();
    }
}